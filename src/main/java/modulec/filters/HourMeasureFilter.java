package modulec.filters;

import modulec.model.Measure;

import java.time.LocalDateTime;
import java.util.function.Predicate;

public class HourMeasureFilter implements Predicate<Measure> {

    private final LocalDateTime hourStart;
    private final LocalDateTime hourEnd;

    public HourMeasureFilter(LocalDateTime hourStart) {
        this.hourStart = hourStart;
        this.hourEnd = hourStart.plusHours(1);
    }

    @Override
    public boolean test(Measure measure) {
        LocalDateTime measureDateTime = measure.getDateTime();
        return measureDateTime.isEqual(hourStart) ||
                measureDateTime.isEqual(hourEnd) ||
                (measureDateTime.isAfter(hourStart) && measureDateTime.isBefore(hourEnd));
    }
}
