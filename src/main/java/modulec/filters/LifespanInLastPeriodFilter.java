package modulec.filters;

import modulec.model.ServerResource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.function.Predicate;

public class LifespanInLastPeriodFilter implements Predicate<ServerResource> {

    private String time = "T23:00:00.000";
    //i assume that is is called at 23:15
    @Override
    public boolean test(ServerResource resource) {
        LocalDate n = LocalDate.now();
        LocalDateTime periodEnd = LocalDateTime.parse(n.toString() + time);
        LocalDateTime periodStart = LocalDateTime.parse(n.minusDays(1).toString() + time);
        String serverStartString = resource.getStartDate().substring(0, resource.getStartDate().length() - 13);
        LocalDateTime serverStart = LocalDateTime.parse(serverStartString);
        LocalDateTime serverEnd;
        if (resource.getEndDate() == null) {
            serverEnd = LocalDateTime.MAX;
        } else {
            serverEnd = LocalDateTime.parse(resource.getEndDate().substring(0, resource.getEndDate().length() - 13));
        }
        return (serverStart.isBefore(periodEnd) && serverEnd.isAfter(periodStart));
    }
}
