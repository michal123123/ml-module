package modulec.services;

import modulec.model.Attributes;
import modulec.model.PlanForADay;
import modulec.model.SupervisedExample;
import org.apache.log4j.Logger;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NnService {
    private static Logger LOGGER = Logger.getLogger(NnService.class);
    private static final int nEpochs = 1000;
    private static int inputFeatures = 7;//+1 for label
    private static final int nSamplesToTrain = 24;
    private static final int nSamplesToGenerate = 24;

    public static MultiLayerNetwork loadNet(File location) throws IOException {
        return ModelSerializer.restoreMultiLayerNetwork(location);
    }

    public static PlanForADay generatePlanFor(MultiLayerNetwork multiLayerNetwork,
                                              LocalDate planDate,
                                              Map<LocalDateTime, Attributes> dateHourToAttributes) {
        List<Integer> vms = new ArrayList<>();

        for (int i = 0; i < nSamplesToGenerate; i++) {
            LocalDateTime localDateTime = LocalDateTime.of(planDate.getYear(), planDate.getMonth(), planDate.getDayOfMonth(), i, 0);
            Attributes attr = dateHourToAttributes.get(localDateTime);
            double[] rowOfFeatures = new double[inputFeatures];
            rowOfFeatures[0] = attr.getHour();
            rowOfFeatures[1] = attr.getHoliday();
            rowOfFeatures[2] = attr.getDayBetweenMondayAndFridayInclusive();
            rowOfFeatures[3] = attr.getSaturday();
            rowOfFeatures[4] = attr.getSunday();
            rowOfFeatures[5] = attr.getSummerVacation();
            rowOfFeatures[6] = attr.getAcademicYear();

            INDArray inputNDArray = Nd4j.create(rowOfFeatures, new int[]{1, inputFeatures}, 'c');//przerabia tablice z pierwsza liczba do sumy na
            INDArray predicted = multiLayerNetwork.output(inputNDArray);
            double d = predicted.getDouble(0);
            if (d < 1) {
                LOGGER.warn("NEGATIVE NUMBERS OF VM");
                d = 1;
            }
            vms.add((int) Math.round(d));
        }
        PlanForADay result = new PlanForADay(planDate, vms);
        return result;
    }


    public static void saveTheNetwork(File locationToSave,
                                      MultiLayerNetwork multiLayerNetwork) throws IOException {
        boolean saveUpdater = true;
        if (locationToSave.exists()) {
            locationToSave.delete();
        }
        ModelSerializer.writeModel(multiLayerNetwork, locationToSave, saveUpdater);
    }


    public static void trainNetwork(MultiLayerNetwork multiLayerNetwork,
                                    List<SupervisedExample> supervisedExamples,
                                    Map<LocalDateTime, Attributes> dateHourToAttributes) {
        DataSet trainDataSet = getTrainData(supervisedExamples, dateHourToAttributes);
        for (int i = 0; i < nEpochs; i++) {
            multiLayerNetwork.fit(trainDataSet);
            LOGGER.info("Epoch " + i + " out of " + nEpochs + " done.");
        }
    }

    private static DataSet getTrainData(List<SupervisedExample> supervisedExamples,
                                        Map<LocalDateTime, Attributes> dateHourToAttributes) {
        if (nSamplesToTrain != supervisedExamples.size()) {
            throw new IllegalArgumentException();
        }
        double[] labels = new double[nSamplesToTrain];
        double[] rowsOfFeatures = new double[nSamplesToTrain * inputFeatures];
        int k = 0;
        for (int i = 0; i < nSamplesToTrain; i++) {
            Attributes attr = dateHourToAttributes.get(supervisedExamples.get(i).getHourStarDateTime());
            rowsOfFeatures[k++] = attr.getHour();
            rowsOfFeatures[k++] = attr.getHoliday();
            rowsOfFeatures[k++] = attr.getDayBetweenMondayAndFridayInclusive();
            rowsOfFeatures[k++] = attr.getSaturday();
            rowsOfFeatures[k++] = attr.getSunday();
            rowsOfFeatures[k++] = attr.getSummerVacation();
            rowsOfFeatures[k++] = attr.getAcademicYear();
            labels[i] = supervisedExamples.get(i).getVmNumber();
        }

        INDArray inputNDArray = Nd4j.create(rowsOfFeatures, new int[]{nSamplesToTrain, inputFeatures}, 'c');//przerabia tablice z pierwsza liczba do sumy na
        INDArray outPut = Nd4j.create(labels, new int[]{nSamplesToTrain, 1});
        return new DataSet(inputNDArray, outPut);
    }
}
