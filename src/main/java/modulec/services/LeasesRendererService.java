package modulec.services;

import modulec.model.Lease;

import java.time.format.DateTimeFormatter;

// lease format:
// 2017-12-10 15:00,2017-12-10 17:00,1,1024,15,lease1,vmIdentifer
public class LeasesRendererService {
    private final static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-dd HH:mm");

    public static String toString(Lease lease) {
        StringBuilder sb = new StringBuilder();
        sb.append(formatter.format(lease.getStart()) + "," + formatter.format(lease.getEnd())
                + "," + 1 + "," + lease.getRam() + ","
                + lease.getDiskGbs() + "," + lease.getLeaseName() + "," + lease.getVmName());
        return sb.toString();
    }
}
