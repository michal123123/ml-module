package modulec.services;

import modulec.PropertiesFromFile;
import modulec.model.ServerResource;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.apache.http.protocol.HTTP.USER_AGENT;

public class TelemetryService {
    private static Logger LOGGER = Logger.getLogger(TelemetryService.class);

    public static List<ServerResource> getServersFromTelemetry(String token) throws IOException {
        String url = PropertiesFromFile.getProperty(PropertiesFromFile.RESOURCE_ENDPOINT);
        HttpURLConnection con = (HttpURLConnection)
                new URL(url).openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("X-Auth-Token", token);
        LOGGER.info("Sending 'GET' request to URL : " + url);
        int responseCode = con.getResponseCode();
        LOGGER.info("Response Code : " + responseCode);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String out = response.toString();
        LOGGER.info("Response:"+out);
        return serversFromJson(out);
    }

    public static List<ServerResource> serversFromJson(String response) {
        String START_DATE_KEY = "started_at";
        String END_DATE_KEY = "ended_at";
        String METRICS_SUB_OBJECT = "metrics";
        String CPU_UTIL_KEY = "cpu_util";
        String ID = "id";
        String ORIGINAL_RESOURCE_ID = "original_resource_id";
        List<ServerResource> serverResources = new ArrayList<>();
        JSONParser parser = new JSONParser();
        try {
            JSONArray servers = (JSONArray) parser.parse(response);
            for (int i = 0; i < servers.size(); i++) {
                JSONObject server = (JSONObject) servers.get(i);
                String start = (String) server.get(START_DATE_KEY);
                String end = (String) server.get(END_DATE_KEY);
                JSONObject metrics = (JSONObject) server.get(METRICS_SUB_OBJECT);
                String cpuUtil = (String) metrics.get(CPU_UTIL_KEY);
                String id = (String) server.get(ID);
                String originalResourceId = (String) server.get(ORIGINAL_RESOURCE_ID);
                ServerResource resource = new ServerResource(start, end, cpuUtil, id, originalResourceId);
                serverResources.add(resource);
            }
        } catch (ParseException e) {
            LOGGER.error(e.getMessage()+e.getStackTrace());
            throw new IllegalArgumentException(e.getMessage());
        }
        return serverResources;
    }
}
