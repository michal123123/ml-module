package modulec.services;

import modulec.PropertiesFromFile;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class TokenService {
    private static Logger LOGGER = Logger.getLogger(TokenService.class);

    public static String getToken() throws IOException {
        String url = PropertiesFromFile.getProperty(PropertiesFromFile.IDENTITY_ENDPOINT) +
                "v2.0/tokens";
        String body = "{\"auth\":{\"passwordCredentials\":{\"username\":\"admin\",\"password\":\"secret\"},\"tenantName\":\"demo\"}}";
        LOGGER.info("POST to"+url+"     "+body);
        URL object = new URL(url);
        HttpURLConnection con = (HttpURLConnection) object.openConnection();
        con.setDoOutput(true);
        con.setDoInput(true);
        con.setRequestProperty("Content-Type", "application/json; charset=utf8");
        con.setRequestProperty("Accept", "application/json");
        con.setRequestMethod("POST");
        OutputStream os = con.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, "UTF-8");
        osw.write(body);
        osw.flush();
        con.connect();
        LOGGER.info(con.getResponseCode());
        LOGGER.info(con.getResponseMessage());
        osw.close();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String out = response.toString();
        LOGGER.info(out);
        return extractId(out);
    }

    private static String extractId(String response) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = (JSONObject) parser.parse(response);
            JSONObject access = (JSONObject) obj.get("access");
            JSONObject token = (JSONObject) access.get("token");
            String id = (String) token.get("id");
            return id;
        } catch (ParseException e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
            throw new IllegalArgumentException();
        }
    }
}
