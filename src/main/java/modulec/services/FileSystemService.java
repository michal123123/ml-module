package modulec.services;

import modulec.model.LeasesForADay;
import modulec.model.PlanForADay;
import modulec.model.SupervisedExample;
import org.apache.log4j.Logger;
import modulec.model.Lease;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

public class FileSystemService {
    private static Logger LOGGER = Logger.getLogger(FileSystemService.class);

    public static void append(File f, String line) throws IOException {
        try (PrintWriter output = new PrintWriter(new FileWriter(f, true))) {
            output.printf("%s\r\n", line);
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
            throw e;
        }
    }

    public static void saveSupervisedExamples(File resultFile, List<SupervisedExample> supervisedExamples) throws IOException {
        if (!resultFile.exists()) {
            resultFile.createNewFile();
        }
        LOGGER.info("Saving superised exampes to " + resultFile.getAbsolutePath());
        for (SupervisedExample example : supervisedExamples) {
            LOGGER.info("Saving" + example);
            FileSystemService.append(resultFile, example.getHourStarDateTime() + "," + example.getVmNumber());
        }
    }

    public static void saveReservationPlan(File f, PlanForADay generatedReservationPlan) throws IOException {
        if (f.exists()) {
            f.delete();
        }
        LeasesForADay leasesForADay = ExamplesToPlanTransformerService.toLeases(generatedReservationPlan);
        try (PrintWriter output = new PrintWriter(new FileWriter(f, true))) {
            Set<Lease> leaseSet = leasesForADay.getLeases();
            for (Lease lease : leaseSet) {
                output.println(LeasesRendererService.toString(lease));
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
            throw e;
        }
    }
}
