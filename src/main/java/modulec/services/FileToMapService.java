package modulec.services;

import modulec.model.Attributes;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileToMapService {

    public static Map<LocalDateTime, Attributes> loadHourToAttributesMap(File dataFile) throws IOException {
        Map<LocalDateTime, Attributes> hourToAttributes = new HashMap<>();
        List<String> lines = Files.readAllLines(Paths.get(dataFile.getAbsolutePath()),
                Charset.defaultCharset());
        for (String line : lines) {
            String[] values = line.split(",");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
            LocalDateTime dateTime = LocalDateTime.parse(values[0], formatter);
            int requests = Integer.parseInt(values[1]);
            final byte hour = Byte.parseByte(values[2]);
            final byte holiday = Byte.parseByte(values[3]);
            final byte dayBetweenMondayAndFridayInclusive = Byte.parseByte(values[4]);
            final byte saturday = Byte.parseByte(values[5]);
            final byte sunday = Byte.parseByte(values[6]);
            final byte summerVacation = Byte.parseByte(values[7]);
            final byte academicYear = Byte.parseByte(values[8]);
            Attributes attr = new Attributes(hour, holiday, dayBetweenMondayAndFridayInclusive, saturday, sunday, summerVacation, academicYear);
            hourToAttributes.put(dateTime, attr);
        }
        return Collections.unmodifiableMap(hourToAttributes);
    }
}
