package modulec.services;

import modulec.PropertiesFromFile;
import modulec.model.*;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import static org.apache.http.protocol.HTTP.USER_AGENT;

public class MetricsRestClientService {
    private static Logger LOGGER = Logger.getLogger(MetricsRestClientService.class);
    private static final String urlEnding = "/measures";

    public static List<ServerAndMetric> getMetrics(List<ServerResource> activeInLastPeriod, String token) throws IOException {
        List<ServerAndMetric> result = new ArrayList<>();
        for (ServerResource server : activeInLastPeriod) {
            Metric metric = getMetricForServer(server, token);
            ServerAndMetric serverAndMetric = new ServerAndMetric(server, metric);
            result.add(serverAndMetric);
        }
        return result;
    }

    private static Metric getMetricForServer(ServerResource server, String token) throws IOException {
        String url = PropertiesFromFile.getProperty(PropertiesFromFile.METRICS_ENDPOINT);
        url = url + server.getCpuUsageMetricId() + urlEnding;
        LOGGER.info("Requesting " + url);
        String output = getHttpRequest(url, token);
        Metric result = parseMetricFromOutput(output);
        LOGGER.info("Parsed metric " + result);
        return result;
    }

    private static Metric parseMetricFromOutput(String response) {
        Measure[] measuresArr;
        JSONParser parser = new JSONParser();
        try {
            JSONArray measures = (JSONArray) parser.parse(response);
            measuresArr = new Measure[measures.size()];
            for (int i = 0; i < measures.size(); i++) {
                JSONArray measure = (JSONArray) measures.get(i);
                String dateTimeString = (String) measure.get(0);
                LocalDateTime dateTime = LocalDateTime.parse(dateTimeString.substring(0, dateTimeString.length() - 6));
                Double granularity = (Double) measure.get(1);
                double value = (double) measure.get(2);
                measuresArr[i] = new Measure(value, dateTime, granularity);
            }
        } catch (ParseException e) {
            LOGGER.error(e.getMessage() + e.getStackTrace());
            throw new IllegalArgumentException();
        }
        Stream<Measure> measureStream = Stream.of(measuresArr);
        Metric result = new Metric(measureStream.collect(Collectors.toList()), MetricType.CPU_UTIL);
        return result;
    }

    private static String getHttpRequest(String url, String token) throws IOException {

        HttpURLConnection con = (HttpURLConnection)
                new URL(url).openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("X-Auth-Token", token);
        int responseCode = con.getResponseCode();
        LOGGER.info("Sending 'GET' request to URL : " + url);
        LOGGER.info("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        String out = response.toString();
        LOGGER.info(out);
        return out;
    }
}