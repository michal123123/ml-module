package modulec.services;

import modulec.model.Lease;
import modulec.model.LeasesForADay;
import modulec.model.PlanForADay;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class ExamplesToPlanTransformerService {

    public static LeasesForADay toLeases(PlanForADay plan) {
        //hour->vm to lease list
        Set<Lease> leaseSet = new HashSet<>();
        List<Integer> modifiablePlan = new ArrayList<>(plan.getVms());
        if (modifiablePlan.size() != 24) {
            throw new IllegalArgumentException();
        }
        LocalDateTime start = findStartTime(plan.getDate(), modifiablePlan);
        do {
            LocalDateTime end = findEnd(modifiablePlan, start);
            leaseSet.add(new Lease(start, end, 15, 1024,
                   "LE"+ UUID.randomUUID().toString().substring(20).replace("-",""),
                    "VM"+UUID.randomUUID().toString().substring(20).replace("-","")));
            decrementVmsNumber(start, end, modifiablePlan);
            start = findStartTime(plan.getDate(), modifiablePlan);
        } while (start != null);
        return new LeasesForADay(leaseSet, plan.getDate());
    }

    private static void decrementVmsNumber(LocalDateTime start, LocalDateTime end, List<Integer> modifiablePlan) {
        int endHour = end.getHour();
        if (end.getHour() == 23 && end.getMinute() == 59) {
            endHour = 24;
        }
        for (int i = start.getHour(); i < endHour; i++) {
            modifiablePlan.set(i, modifiablePlan.get(i) - 1);
        }
    }

    private static LocalDateTime findEnd(List<Integer> modifiablePlan, LocalDateTime start) {
        int vmsNumber = modifiablePlan.get(start.getHour());
        for (int i = start.getHour(); i < 24; i++) {
            if (modifiablePlan.get(i) < vmsNumber) {
                return start.withHour(i);
            }
        }
        return start.withHour(23).withMinute(59);
    }

    private static LocalDateTime findStartTime(LocalDate date, List<Integer> vmsForHours) {
        for (int i = 0; i < vmsForHours.size(); i++) {
            if (vmsForHours.get(i) > 0) {
                return LocalDateTime.of(date.getYear(), date.getMonth(),
                        date.getDayOfMonth(), i, 0);
            }
        }
        return null;
    }
}
