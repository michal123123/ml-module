package modulec.model;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Measure implements Serializable{
    private final double value;
    private final LocalDateTime dateTime;
    private double granularitySeconds;

    public Measure(double value, LocalDateTime dateTime, double granularitySeconds) {
        this.value = value;
        this.dateTime = dateTime;
        this.granularitySeconds = granularitySeconds;
    }

    public double getValue() {
        return value;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public double getGranularitySeconds() {
        return granularitySeconds;
    }

    @Override
    public String toString() {
        return "Measure{" +
                "value=" + value +
                ", dateTime=" + dateTime +
                ", granularitySeconds=" + granularitySeconds +
                '}';
    }
}
