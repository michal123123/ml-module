package modulec.model;


import java.time.LocalDateTime;

//2017-12-10 15:00,2017-12-10 17:00,1,1024,15,lease1,vmMichala1
//describes lease for a single vm
public class Lease {

    private final LocalDateTime start;
    private final LocalDateTime end;
    private final int diskGbs;
    private final String leaseName;
    private final String vmName;
    private final int ram;

    public Lease(LocalDateTime start, LocalDateTime end, int diskGbs, int ram,String leaseName,
                 String vmName ) {
        this.start = start;
        this.end = end;
        this.diskGbs = diskGbs;
        this.leaseName = leaseName;
        this.vmName = vmName;
        this.ram = ram;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public int getDiskGbs() {
        return diskGbs;
    }

    public String getLeaseName() {
        return leaseName;
    }

    public String getVmName() {
        return vmName;
    }

    public int getRam() {
        return ram;
    }
}
