package modulec.model;

import java.time.LocalDateTime;

public class SupervisedExample {

    private final int vmNumber;
    private final LocalDateTime hourStarDateTime;

    public SupervisedExample(int vmNumber, LocalDateTime hourStarDateTime) {
        this.vmNumber = vmNumber;
        this.hourStarDateTime = hourStarDateTime;
    }

    public int getVmNumber() {
        return vmNumber;
    }

    public LocalDateTime getHourStarDateTime() {
        return hourStarDateTime;
    }

    @Override
    public String toString() {
        return "SupervisedExample{" +
                "vmNumber=" + vmNumber +
                ", hourStarDateTime=" + hourStarDateTime +
                '}';
    }
}
