package modulec.model;

import java.io.Serializable;

public class ServerAndMetric implements Serializable {
    private final ServerResource server;
    private final Metric cpuUtil;

    public ServerAndMetric(ServerResource server, Metric cpuUtil) {
        this.server = server;
        this.cpuUtil = cpuUtil;
    }

    public ServerResource getServer() {
        return server;
    }

    public Metric getCpuUtil() {
        return cpuUtil;
    }

    @Override
    public String toString() {
        return "ServerAndMetric{" +
                "server=" + server +
                ", cpuUtil=" + cpuUtil +
                '}';
    }
}
