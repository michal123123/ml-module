package modulec.model;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class PlanForADay {
    private final LocalDate date;
    private final List<Integer> vms;

    public PlanForADay(LocalDate date, List<Integer> vms) {
        this.date = date;
        if (vms.size() != 24) {
            throw new IllegalArgumentException();
        }
        this.vms = Collections.unmodifiableList(vms);
    }

    public LocalDate getDate() {
        return date;
    }

    public List<Integer> getVms() {
        return vms;
    }

    @Override
    public String toString() {
        return "PlanForADay{" +
                "date=" + date +
                ", vms=" + vms +
                '}';
    }
}
