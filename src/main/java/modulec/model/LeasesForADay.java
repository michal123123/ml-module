package modulec.model;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class LeasesForADay {
    private final Set<Lease> leases;
    private final LocalDate date;

    public LeasesForADay(Set<Lease> leases, LocalDate date) {
        this.leases = Collections.unmodifiableSet(new HashSet<>(leases));
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    public Set<Lease> getLeases() {
        return leases;
    }
}
