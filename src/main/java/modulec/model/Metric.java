package modulec.model;

import java.io.Serializable;
import java.util.List;

public class Metric implements Serializable {
    private final List<Measure> measures;
    private final MetricType type;

    public Metric(List<Measure> measureStream, MetricType type) {
        this.measures = measureStream;
        this.type = type;
    }

    public List<Measure> getMeasures() {
        return measures;
    }

    @Override
    public String toString() {
        return "Metric{" +
                "measures=" + measures +
                ", type=" + type +
                '}';
    }
}
