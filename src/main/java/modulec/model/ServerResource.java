package modulec.model;


import javax.annotation.Nullable;
import java.io.Serializable;

public class ServerResource implements Serializable {
    private String cpuUsageMetricId;
    private String startDate;
    private String id;
    private String originalResourceId;
    private String endDate;

    public ServerResource(String startDate, String endDate, String cpuUsageMetricId,
                          String id, String originalResourceId) {
        this.cpuUsageMetricId = cpuUsageMetricId;
        this.startDate = startDate;
        this.id = id;
        this.originalResourceId = originalResourceId;
        this.endDate = endDate;
    }

    public String getCpuUsageMetricId() {
        return cpuUsageMetricId;
    }

    public String getStartDate() {
        return startDate;
    }

    @Nullable
    public String getEndDate() {
        return endDate;
    }

    public String getId() {
        return id;
    }

    public String getOriginalResourceId() {
        return originalResourceId;
    }

    @Override
    public String toString() {
        return "ServerResource{" +
                "cpuUsageMetricId='" + cpuUsageMetricId + '\'' +
                ", startDate='" + startDate + '\'' +
                ", id='" + id + '\'' +
                ", originaResourceId='" + originalResourceId + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
