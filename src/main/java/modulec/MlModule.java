package modulec;

import modulec.filters.HourMeasureFilter;
import modulec.filters.LifespanInLastPeriodFilter;
import modulec.model.*;
import modulec.services.*;
import org.apache.log4j.Logger;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MlModule {
    private static Logger LOGGER = Logger.getLogger(MlModule.class);
    public static String WORKSPACE = System.getenv("OPENSTACK_WORKSPACE");
    private static double recommededPercentage;

    public static void main(String[] args) throws IOException {
        LOGGER.info("Module C starting ...");
        //load properties
        File propertiesFile = new File(WORKSPACE+"/module_c_properties");
        LOGGER.info("Loading properties from file " + propertiesFile.getAbsolutePath());
        PropertiesFromFile.getInstance(propertiesFile);
        LOGGER.info("Properties loaded successfully");
        recommededPercentage = (
                Integer.parseInt(PropertiesFromFile.getProperty(PropertiesFromFile.R_MAX)) +
                Integer.parseInt(PropertiesFromFile.getProperty(PropertiesFromFile.R_MIN))
                                )/2;

        //load data file to obtain each hour ML attributes
        File dataFile = new File(WORKSPACE+"/pastUsageData");
        LOGGER.info("Loading date attributes from file " + dataFile.getAbsolutePath());
        Map<LocalDateTime, Attributes> attributesMap = FileToMapService.loadHourToAttributesMap(dataFile);
        LOGGER.info("Date-to-MlAttributes map loaded.");

        //generate auth token
        LOGGER.info("Requesting auth token...");
        String token = TokenService.getToken();
        LOGGER.info("Obtained token:" + token);

        //get all server resources from telemetry
        LOGGER.info("Obtaining server resources from " + PropertiesFromFile.RESOURCE_ENDPOINT);
        List<ServerResource> serverResources = TelemetryService.getServersFromTelemetry(token);

        LOGGER.info("Filtering server to only last cycle...");
        //filter servers active in last period
        List<ServerResource> activeInLastPeriod =
                serverResources.stream().filter(new LifespanInLastPeriodFilter()).
                        collect(Collectors.toList());
        logServers(activeInLastPeriod);

        //get metrics for active servers
        LOGGER.info("Requesting metrics for active servers...");
        List<ServerAndMetric> serversAndMetrics =
                MetricsRestClientService.getMetrics(activeInLastPeriod, token);
        LOGGER.info("Metrics downloaded for filtered VMs.");

        //generate supervised examples
        LOGGER.info("Generating supervised examples...");
        List<SupervisedExample> supervisedExamples = generateSupervisedExamples(serversAndMetrics);
        LOGGER.info("Supervised examples generated.");

        //save supervised examples to file
        File resultFile = new File(WORKSPACE+"/supervised_examples_generated_at" + LocalDate.now());
        FileSystemService.saveSupervisedExamples(resultFile, supervisedExamples);
        LOGGER.info("Supervised examples saved successfully.");

        //load NN
        File networkFile = new File(WORKSPACE+"/network_dl4j.zip");
        LOGGER.info("Loading DL4J network from " + networkFile.getAbsolutePath());
        MultiLayerNetwork net = NnService.loadNet(networkFile);
        LOGGER.info("Network loaded succesfully.");

        //train ML netowrk
        LOGGER.info("Training network on " + supervisedExamples.size() + " examples...");
        NnService.trainNetwork(net, supervisedExamples, attributesMap);
        LOGGER.info("Training network finished.");

        //generate reservation plan for next cycle
        LocalDate tomorrowDate = LocalDate.now().plusDays(1);
        LOGGER.info("Generating reservation plan for tomorrow, i.e. " + tomorrowDate.toString());
        PlanForADay generatedReservationPlan =
                NnService.generatePlanFor(net, tomorrowDate, attributesMap);
        LOGGER.info("Generated plan " + generatedReservationPlan);

        // and save this plan
        File f = new File(WORKSPACE+"/planFor" + tomorrowDate.toString());
        LOGGER.info("Saving generated plan to " + f.getAbsolutePath());
        FileSystemService.saveReservationPlan(f, generatedReservationPlan);

        //save NN
        LOGGER.info("Saving net to " + networkFile.getAbsolutePath());
        NnService.saveTheNetwork(networkFile, net);
        LOGGER.info("Net saved. Module C has finished its work. Exiting...");
    }

    private static void logServers(List<ServerResource> activeInLastPeriod) {
        LOGGER.info("Selected " + activeInLastPeriod.size() + " server resources:");
        for (ServerResource serverResource : activeInLastPeriod) {
            LOGGER.info(serverResource);
        }
        LOGGER.info("------------------------------------------------------");
    }

    private static List<SupervisedExample> generateSupervisedExamples(List<ServerAndMetric> serversAndMetrics) {
        List<SupervisedExample> results = new ArrayList<>();
        LocalDate now = LocalDate.now();
        String time = "T23:00:00.000";
        LocalDateTime periodStartTime = LocalDateTime.parse(now.minusDays(1).toString() + time);
        for (int i = 0; i < 24; i++) {
            LocalDateTime currHourStartTime = periodStartTime.plusHours(i);
            int vms = getCorrectVmsNumberForHour(currHourStartTime, serversAndMetrics);
            results.add(new SupervisedExample(vms, currHourStartTime));
        }
        return results;

    }

    private static int getCorrectVmsNumberForHour(LocalDateTime currHour, List<ServerAndMetric> serversAndMetrics) {
        double cpuPercentageSumForHour = 0;
        for (ServerAndMetric serverAndMetric : serversAndMetrics) {
            cpuPercentageSumForHour += getAverageCpuPercentageFromVM(currHour, serverAndMetric.getCpuUtil());
        }
        int vmCount = (int) Math.round(cpuPercentageSumForHour / recommededPercentage);
        return Math.max(vmCount, 1);//1 is the minimum

    }

    private static double getAverageCpuPercentageFromVM(LocalDateTime currHour, Metric cpuUtil) {
        //here we have chosen vm and hour, so we select metrics in this hour for this vm
        //sum them and divide by their number
        List<Measure> measuresInCurrHour =
                cpuUtil.getMeasures().stream().filter(new HourMeasureFilter(currHour)).collect(Collectors.toList());
        double result = 0;
        if(measuresInCurrHour.size() == 0) return 0;
        for (Measure m : measuresInCurrHour) {
            result += m.getValue();
        }
        LOGGER.info("getAverageCpuPercentageFromVM() returned " + result / (double) measuresInCurrHour.size() +
                " for hour " + currHour.toString() + " and metric " + cpuUtil.toString());
        return result / (double) measuresInCurrHour.size();
    }

}
